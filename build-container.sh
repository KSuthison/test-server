#! /bin/sh

yarn build
docker build -t node-server .
docker run -p 8888:8888 -d --name test-server node-server