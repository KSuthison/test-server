import AuthHelper from '../../src/helper/authHelper'

test('check isvalid email aaa@bbb.com', () => {
    const input = "aaa@bbb.com", expected = true
    var output = AuthHelper.isValidEmail(input)
    expect(output).toBe(expected);
  });

test('check isvalid email aaa.bbb', () => {
    const input = "aaa.bbb", expected = false
    var output = AuthHelper.isValidEmail(input)
    expect(output).toBe(expected)
})

//bcrypt did not return the same result 
// test('hash password 123', () => {
//     const input = "123", expected = "$2b$08$rcE/1mFZBpE6wfw6i1AhzO9HVZrvjY2Zd7RwaIahhvcGgiFjPWpAq"
//     var output = AuthHelper.hashPassword(input)
//     expect(output).toBe(expected)
// })

test('compare password with hash password 123', () => {
    const inputPassword = "123", inputHashPassword = "$2b$08$ho1jlEsSeuL/tJ87.soFUOZPn29c22/ld9geR9Qau5ekbcHL2yGyW", expected = true
    var output = AuthHelper.comparePassword(inputPassword,inputHashPassword)
    expect(output).toBe(expected)
})

//jwt did not return the same result 
// test('generate token with ID 11223344', () => {
//     const input = "11223344", expected = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxMTIyMzM0NCIsImlhdCI6MTU3NDIyMzI1MSwiZXhwIjoxNTc0ODI4MDUxfQ.NkFEg-pDj-wwmgDe88ACD34TKxj8fwmWBwFS0WEajbQ"
//     var output = AuthHelper.generateToken(input)
//     expect(output).toBe(expected)
// })