import contentRoute from './content';
import config from 'config';
import userRoute from './user';
import { verifyToken } from '../controllers/auth/auth'

export function initRouter(server) {

    //log access route
    server.post('*', function (req, res, next) {
        console.log('Request was made to: ' + req.originalUrl);
        return next();
    });

    //get app version
    server.get('/', function (req, res) {
        console.log('get app vesion')
        return res.status(200).send({ app_version: config.app.version });
    });

    //login and register
    server.use('/user', userRoute);

    //verify token before access to content route
    server.use('/content', verifyToken, contentRoute);

}