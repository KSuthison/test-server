import { Router } from 'express';
import {createContent} from '../controllers/content/create';
import {deleteContent} from '../controllers/content/delete';
import {updateContent} from '../controllers/content/update';
import {getContent} from '../controllers/content/get';
import {getAllContents} from '../controllers/content/getall';

let router = Router();

router.post('/create', createContent);
router.post('/delete', deleteContent);
router.post('/update', updateContent);
router.post('/get', getContent);
router.post('/getall', getAllContents);

export default router;