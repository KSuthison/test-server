import { Router } from 'express';
import { login } from '../controllers/user/login';
import {register} from '../controllers/user/register';

let router = Router();

router.post('/login', login);
router.post('/register', register);

export default router;