import PgConnector from '../../services/pgConnector'

/**
   * Get a content
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object 
   */
export async function getAllContents(req, res) {

    const query = PgConnector.query

    const getQuery = `SELECT * from contents WHERE owner_id = $1`;
    const values = [
        req.user.id
    ];

    let promise = query(getQuery, values)
    promise.then(
        resolve => {
            console.log(resolve)
            return res.status(201).send(resolve.rows)
        },
        reject => {
            return res.status(400).send(reject)
        }
    )
}