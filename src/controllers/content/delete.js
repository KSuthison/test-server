import PgConnector from '../../services/pgConnector'
import uuidv4 from 'uuid/v4'

/**
   * Delete a content
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object 
   */
export async function deleteContent(req, res) {
    const query = PgConnector.query
    const deleteQuery = `DELETE FROM contents WHERE owner_id = $1 AND id=$2 returning *`;
    const values = [
        req.user.id,
        req.body.contentID
    ];

    try {
        const { rows } = await query(deleteQuery, values);
        return res.status(201).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(400).send(error);
    }
}