import PgConnector from '../../services/pgConnector'

/**
   * Get a content
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object 
   */
export async function getContent(req, res) {

    const query = PgConnector.query

    const getQuery = `SELECT * from contents WHERE owner_id = $1 AND id = $2`;
    const values = [
        req.user.id,
        req.body.contentID
    ];

    // try {
    //     const { rows } = await query(getQuery, values);
    //     return res.status(201).send(rows[0]);
    // } catch (error) {
    //     console.log(error)
    //     return res.status(400).send(error);
    // }
    let promise = query(getQuery, values)
    promise.then(
        resolve => {
            console.log(resolve)
            return res.status(201).send(resolve.rows[0])
        },
        reject => {
            return res.status(400).send(reject)
        }
    )
}