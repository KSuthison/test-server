import PgConnector from '../../services/pgConnector'
import uuidv4 from 'uuid/v4'

/**
   * Create a content
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object 
   */
export async function createContent(req, res) {
    const query = PgConnector.query
    const createQuery = `INSERT INTO contents(id,name,status,content,category,author,owner_id,created_date) values ($1,$2,$3,$4,$5,$6,$7,$8)`;
    const values = [
        uuidv4(),
        req.body.name,
        req.body.status,
        req.body.content,
        req.body.category,
        req.body.author,
        req.user.id,
        new Date()
    ];

    // try {
    //     const { rows } = await query(createQuery, values);
    //     return res.status(201).send(rows[0]);
    // } catch (error) {
    //     return res.status(400).send(error);
    // }
    let promise = query(createQuery, values)
    promise.then(
        result => {
            return res.status(201).send(result.rows[0])
        },
        error => {
            console.log(error)
            return res.status(400).send(error)
        }
    )
}