import PgConnector from '../../services/pgConnector'

/**
   * Get a content
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object 
   */
export async function updateContent(req, res) {

    const query = PgConnector.query

    const getQuery = `UPDATE contents SET name=$1,status=$2,content=$3,category=$4,author=$5,modified_date=$6 WHERE owner_id=$7 and id=$8 RETURNING *`;
    const values = [
        req.body.name,
        req.body.status,
        req.body.content,
        req.body.category,
        req.body.author,
        new Date(),
        req.user.id,
        req.body.contentID
    ];

    let promise = query(getQuery, values)
    promise.then(
        resolve => {
            console.log(resolve)
            return res.status(201).send(resolve.rows)
        },
        reject => {
            console.log(reject)
            return res.status(400).send(reject)
        }
    )
}