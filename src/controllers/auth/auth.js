import PgConnector from '../../services/pgConnector'
import jwt from 'jsonwebtoken';
import config from 'config'
/**
   * Verify Token
   * @param {object} req 
   * @param {object} res 
   * @param {object} next
   * @returns {object|void} response object 
   */
  export async function verifyToken(req, res, next) {
    const query = PgConnector.query
    const token = req.headers['x-access-token'];
    if (!token) {
      return res.status(400).send({ 'message': 'Token is not provided' });
    }
    try {
      const decodedContentFromToken = jwt.verify(token, process.env.SECRET);
      const queryText = 'SELECT * FROM users WHERE id = $1';
      const { rows } = await query(queryText, [decodedContentFromToken.userId]);
      if (!rows[0]) {
        return res.status(400).send({ 'message': 'The token you provided is invalid' });
      }
      req.user = { id: decodedContentFromToken.userId };
      next();
    } catch (error) {
        console.log(error)
      return res.status(400).send(error);
    }
  }