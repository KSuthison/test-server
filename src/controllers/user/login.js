import PGConnector from '../../services/pgConnector'
import AuthHelper from '../../helper/authHelper'
import ErrorHelper from '../../helper/errorHelper'

/**
   * Login
   * @param {object} req 
   * @param {object} res
   * @returns {object} user object 
   */
export async function login(req, res) {

    const query = PGConnector.query

    //missing parameter
    if (!req.body.username || !req.body.password) {
        let error = ErrorHelper.getErrorResponse(ErrorHelper.errors.LOGIN.MISSING_VALUES)
        return res.status(400).send(error);
    }
    //Check username format
    // if (!AuthHelper.isValidEmail(req.body.email)) {
    //   return res.status(400).send({ 'message': 'Please enter a valid email address' });
    // }
    const queryText = 'SELECT * FROM users WHERE username = $1';
    try {
        const { rows } = await query(queryText, [req.body.username]);
        console.log(rows)
        //not existed user but return as incorrect credentials
        if (!rows[0]) {
            let error = ErrorHelper.getErrorResponse(ErrorHelper.errors.LOGIN.INCORRECT_CREDENTIALS)
            return res.status(400).send(error);
        }
        //incorrect credentials
        if (!AuthHelper.comparePassword(req.body.password,rows[0].password)) {
            let error = ErrorHelper.getErrorResponse(ErrorHelper.errors.LOGIN.INCORRECT_CREDENTIALS)
            return res.status(400).send(error);
        }
        const token = AuthHelper.generateToken(rows[0].id);
        return res.status(200).send({ token });
    } catch (error) {
        console.log(error)
        return res.status(400).send(error)
    }
}
