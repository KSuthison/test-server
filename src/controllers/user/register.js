import AuthHelper from '../../helper/authHelper'
import ErrorHelper from '../../helper/errorHelper'

import uuidv4 from 'uuid/v4'
import PgConnector from '../../services/pgConnector'


/**
* Create A User
* @param {object} req 
* @param {object} res
* @returns {object} reflection object 
*/
export async function register(req, res) {

  if (!req.body.username || !req.body.password) {
    let error = ErrorHelper.getErrorResponse(ErrorHelper.errors.REGISTER.MISSING_VALUES)
    return res.status(400).send(error);
  }

  const query = PgConnector.query

  const hashPassword = AuthHelper.hashPassword(req.body.password);

  const createQuery = `INSERT INTO
      users(id, username, password, created_date)
      VALUES($1, $2, $3, $4)
      returning *`;
  const values = [
    uuidv4(),
    req.body.username,
    hashPassword,
    new Date()
  ];

  try {
    const { rows } = await query(createQuery, values);
    const token = AuthHelper.generateToken(rows[0].id);
    return res.status(201).send({ token });
  } catch (error) {
    console.log(error)
    if (error.routine === '_bt_check_unique') {
      let error = ErrorHelper.getErrorResponse(ErrorHelper.errors.REGISTER.DUPLICATE_USERNAME)
      return res.status(400).send(error)
    }
    return res.status(400).send(error);
  }
}
