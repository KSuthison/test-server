
const errors = {
    LOGIN: {
        INCORRECT_CREDENTIALS: "The credentials you provided is incorrect",
        MISSING_VALUES: "Some values are missing"
    },
    REGISTER: {
        DUPLICATE_USERNAME: "User with that Username already exist",
        MISSING_VALUES: "Some values are missing"
    },
}

function getErrorResponse(error) {
    let response = { message: '' }
    if (!error) {
        return null;
    }
    response.message = error
    return response
}

export default {
    errors,
    getErrorResponse
};
