const Sequelize = require('sequelize');
const config = require('config');

// Option 1: Passing parameters separately
const sequelize = new Sequelize(config.postgres.database
    , config.postgres.user
    , config.postgres.password
    , {
        host: config.postgres.host,
        port: config.postgres.port,
        dialect: 'postgres' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
    },
    {
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    });
// Option 2: Passing a connection URI
// const sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname');

const testConnection = () => {
    sequelize
        .authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
        });
}

export default {
    testConnection,
    sequelize
}
