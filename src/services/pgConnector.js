import { Pool } from 'pg';
import config from 'config'

const pool = new Pool({
  user: config.postgres.user,
  host: config.postgres.host,
  database: config.postgres.database,
  password: config.postgres.password,
  port: config.postgres.port
});

pool.on('connect', () => {
  console.log('connected to the db');
});

// pool.query('SELECT NOW()', (err, res) => {
//     console.log(err, res)
//     pool.end()
//   })

const createUserTable = () => {
  const queryText = `CREATE TABLE IF NOT EXISTS
  users(
    id UUID PRIMARY KEY,
    username VARCHAR(128) UNIQUE NOT NULL,
    password VARCHAR(128) NOT NULL,
    created_date TIMESTAMP
  )`
  return new Promise((resolve, reject) => {
    pool.query(queryText)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      })
  })
}

const createContentTable = () => {
  const queryText = `CREATE TABLE IF NOT EXISTS
      contents(
        id UUID PRIMARY KEY,
        name TEXT NOT NULL,
        status TEXT NOT NULL,
        content TEXT NOT NULL,
        category TEXT NOT NULL,
        author TEXT NOT NULL,
        owner_id UUID NOT NULL,
        created_date TIMESTAMP,
        modified_date TIMESTAMP,
        FOREIGN KEY (owner_id) REFERENCES users (id) ON DELETE CASCADE
      )`
  return new Promise((resolve, reject) => {
    pool.query(queryText)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      })
  })
}

// const createUserTable = () => {
//   const queryText =
//     `CREATE TABLE IF NOT EXISTS
//         users(
//           id UUID PRIMARY KEY,
//           username VARCHAR(128) UNIQUE NOT NULL,
//           password VARCHAR(128) NOT NULL,
//           created_date TIMESTAMP
//         )`;

//   pool.query(queryText)
//     .then((res) => {
//       // console.log(res);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// }

// const createContentTable = () => {
//   const queryText =
//     `CREATE TABLE IF NOT EXISTS
//     contents(
//       id UUID PRIMARY KEY,
//       name TEXT NOT NULL,
//       status TEXT NOT NULL,
//       content TEXT NOT NULL,
//       category TEXT NOT NULL,
//       author TEXT NOT NULL,
//       owner_id UUID NOT NULL,
//       created_date TIMESTAMP,
//       modified_date TIMESTAMP,
//       FOREIGN KEY (owner_id) REFERENCES users (id) ON DELETE CASCADE
//     )`;
//   pool.query(queryText)
//     .then((res) => {
//       // console.log(res);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// }

/**
 * DB Query
 * @param {string} text
 * @param {Array} params
 * @returns {object} object 
 */
const query = (text, params) => {
  return new Promise((resolve, reject) => {
    pool.query(text, params)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      })
  })
}

const createAllTables = () => {
  createUserTable();
  createContentTable();
}

export default {
  createUserTable,
  createContentTable,
  query
}
  // pool.on('remove', () => {
  //   console.log('client removed');
  //   process.exit(0);
  // });

