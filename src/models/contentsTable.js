import Sequelize from 'sequelize'
import PgConnectorOrm from '../services/pgConnectorOrm'
const Model = Sequelize.Model;

const sequelize = PgConnectorOrm.sequelize
class ContentsTable extends Model {}
ContentsTable.init({
    // attributes
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    status: {
        type: Sequelize.STRING,
        allowNull: false
    },
    content: {
        type: Sequelize.STRING,
        allowNull: false
    },
    category: {
        type: Sequelize.STRING,
        allowNull: false
    },
    author: {
        type: Sequelize.STRING,
        allowNull: false
    },
    owner_id: {
        type: Sequelize.UUID,
        allowNull: false
    },
    created_date: {
        type: Sequelize.DATE,
        // allowNull defaults to true
    },
    modified_date: {
        type: Sequelize.DATE,
        // allowNull defaults to true
    }
}, {
    sequelize,
    modelName: 'contents',
    timestamps: false
    // options
});
export default ContentsTable
