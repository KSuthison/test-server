import Sequelize from 'sequelize'
import PgConnectorOrm from '../services/pgConnectorOrm'
import ContentsTable from './contentsTable';
const Model = Sequelize.Model;

const sequelize = PgConnectorOrm.sequelize

class UsersTable extends Model {}
UsersTable.init({
    // attributes
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false
    },
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    created_date: {
        type: Sequelize.DATE,
        // allowNull defaults to true
    },
}, {
    sequelize,
    modelName: 'users',
    timestamps: false
    // options
});
ContentsTable.belongsTo(UsersTable, {foreignKey: 'owner_id',targetKey:'id'})
export default UsersTable


