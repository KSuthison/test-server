
import express from 'express';
import config from 'config';
import { initRouter } from './routes/';
import PgConnector from './services/pgConnector';
import PgConnectorOrm from './services/pgConnectorOrm'
import UsersTable from './models/usersTable'
import ContentsTable from './models/contentsTable'
import cors from 'cors'

// PgConnectorOrm.testConnection();

// let promise = PgConnector.createUserTable()
//     promise.then(
//         result => {
//             PgConnector.createContentTable()
//         },
//         error => {
//             console.log(error)
//         }
//     )

// Note: using `force: true` will drop the table if it already exists
UsersTable.sync({ force: false }).then(() => {
    ContentsTable.sync({ force: false })
})


const app = express()

app.use(cors())
//call middleware to parse body to json
app.use(express.json())


//initial router
initRouter(app)
//initial database

app.listen(config.app.port)
console.log(`listening on port ${config.app.port}`)
