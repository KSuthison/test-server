# Test Server
This test server is implimented using Node.js

project folder structure ([reference](https://medium.com/codebase/structure-of-a-nodejs-api-project-cdecb46ef3f8))

## Get started

yarn build; 
yarn start;

## Module used

- use config module
yarn add config
config module will find config folder at root folder by default

- use express module
yarn add express

- nodejs runtime cannot understand some of the ES6 features we used so we babel
yarn add @babel/core @babel/cli
yarn add @babel/preset-env
don't for get to create .babelrc at root folder

at this point you can not use async and await it will throw err "regeneratorRuntime is not defined"
from babel github in this issue 
theere is an answer to use 

yarn add @babel/plugin-transform-runtime --dev;
yarn add @babel/runtime;

for the dependencies and having the following in .babelrc

"plugins": [
    ["@babel/plugin-transform-runtime",
      {
        "regenerator": true
      }
    ]
  ],


- bcrypt
yarn add bcrypt
algorithm Bcrypt encryption with Eksblowfish Algorithm as a one-way hashing algorithm.

- jsonwebtoken (jwt) 
yarn add jsonwebtoken

-uuid
to generate unique id 

-pm2
yarn add pm2
because node is single thread so will we use pm2 which is 
a daemon process manager that will help you manage and keep your application online.

generate ecosystem file 
yarn pm2 ecosystem
This will generate ecosystem.config.js
then you can call 
yarn pm2 [start|restart|stop|delete] ecosystem.config.js

## reminder
- function work with db connection has to be async and use await when get query from db
- middleware can chain req and res value (look at auth which pass userid with req value)

## problems found during developing 

### Cross-Origin Request Blocked with React and Express


There's a node package called cors which makes it very easy.

$yarn add cors

const cors = require('cors')

app.use(cors())

You don't need any config to allow all.


Gitlab CI CD

https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster