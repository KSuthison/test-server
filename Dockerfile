FROM node:latest

WORKDIR /user/src/app

COPY package.json ./

RUN yarn

RUN yarn add pm2 -g

COPY . .

EXPOSE 8888

RUN yarn start

CMD [ "yarn","pm2-runtime", "start", "ecosystem.config.js" ]